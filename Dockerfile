FROM phpunit/phpunit:5.7.12

COPY . /black_rabbit

ENTRYPOINT phpunit /black_rabbit/test
